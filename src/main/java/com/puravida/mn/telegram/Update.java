package com.puravida.mn.telegram;

import io.micronaut.core.annotation.Introspected;

@Introspected
public class Update {

    int update_id;

    UserMessage message;

    CallbackQuery callback_query;

    public int getUpdate_id() {
        return update_id;
    }

    public void setUpdate_id(int update_id) {
        this.update_id = update_id;
    }

    public UserMessage getMessage() {
        return message;
    }

    public void setMessage(UserMessage message) {
        this.message = message;
    }

    public CallbackQuery getCallback_query() {
        return callback_query;
    }

    public void setCallback_query(CallbackQuery callback_query) {
        this.callback_query = callback_query;
    }
}
