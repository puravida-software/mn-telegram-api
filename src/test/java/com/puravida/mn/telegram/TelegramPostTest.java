package com.puravida.mn.telegram;

import com.puravida.mn.telegram.util.DialogService;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.inject.Singleton;

@MicronautTest
public class TelegramPostTest {

    @Inject
    @Client("/${telegram.token}")
    RxHttpClient client;

    @Inject
    DialogServiceTest dialogServiceTest;

    @Test
    void testPostUpdate(){

        Update update = new Update();
        UserMessage message = new UserMessage();
        update.setMessage(message);

        final boolean ret = client.toBlocking().retrieve(HttpRequest.POST("/", update), Boolean.class);

        assert ret;
        assert dialogServiceTest.receiveAnswer;
    }

    @Test
    void testPostCallbackUpdate(){

        Update update = new Update();
        CallbackQuery callbackQuery = new CallbackQuery();
        update.setCallback_query(callbackQuery);

        final boolean ret = client.toBlocking().retrieve(HttpRequest.POST("/", update), Boolean.class);

        assert ret;
        assert dialogServiceTest.receiveAnswer == false;
    }

}
