package com.puravida.mn.telegram;

import com.puravida.mn.telegram.util.DialogService;

import javax.inject.Singleton;

@Singleton
public class DialogServiceTest implements DialogService {

    boolean receiveAnswer=false;

    void init(){

    }

    @Override
    public void answer(Update update) {
        receiveAnswer = true;
        System.out.println("answer");
    }

    @Override
    public void answerCallback(Update udpate) {
        receiveAnswer = false;
        System.out.println("callback");
    }
}
