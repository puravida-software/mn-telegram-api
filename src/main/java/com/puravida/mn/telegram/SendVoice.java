package com.puravida.mn.telegram;

public class SendVoice extends SendMedia{

    @Override
    protected String getMedia() {
        return "voice";
    }

    @Override
    protected String getSuffix() {
        return "ogg";
    }
}
