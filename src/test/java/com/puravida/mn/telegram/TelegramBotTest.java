package com.puravida.mn.telegram;

import io.micronaut.runtime.server.EmbeddedServer;
import io.micronaut.test.annotation.MicronautTest;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;

@MicronautTest
public class TelegramBotTest {

    @Inject
    EmbeddedServer server;

    @Inject
    TelegramBot telegramBot;

    @Test
    void testHello(){
        Message message = new Message();
        message.setChat_id("-1001382561920");
        message.setText("hola from telegram api");
        telegramBot.sendMessage(message).blockingGet();
    }

    @Test
    void testPhoto() throws IOException {
        SendPhoto sendPhoto = new SendPhoto();
        sendPhoto.setChat_id("-1001382561920");
        sendPhoto.setFile(new File("tests/test.png"));
        telegramBot.sendPhoto(sendPhoto.multipartBody()).blockingGet();
    }

}
