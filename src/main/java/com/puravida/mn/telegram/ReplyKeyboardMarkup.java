package com.puravida.mn.telegram;

import java.util.ArrayList;
import java.util.List;
import io.micronaut.core.annotation.Introspected;

@Introspected
public class ReplyKeyboardMarkup extends ReplyMarkup{
    List<List<KeyboardButton>> keyboard = new ArrayList<>();

    public List<List<KeyboardButton>> getKeyboard() {
        return keyboard;
    }

    public void setKeyboard(List<List<KeyboardButton>> keyboard) {
        this.keyboard = keyboard;
    }
}
