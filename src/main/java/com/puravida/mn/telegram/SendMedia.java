package com.puravida.mn.telegram;

import io.micronaut.http.MediaType;
import io.micronaut.http.client.multipart.MultipartBody;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

abstract public class SendMedia {
    String chat_id;
    String caption;
    byte[] bytes;

    File file;
    public void getFile(){
        throw new RuntimeException("no file needed");
    }

    public void setFile( File f) throws IOException {
        caption = f.getName().split("\\.")[0];
        bytes = Files.readAllBytes(Paths.get(f.getAbsolutePath()));
    }

    abstract protected String getMedia();
    abstract protected String getSuffix();

    public MultipartBody multipartBody(){
        return MultipartBody.builder()
                .addPart("chat_id", chat_id)
                .addPart("caption", caption !=null ? caption : "")
                .addPart(getMedia(), (caption !=null ? caption : getMedia())+"."+getSuffix(), MediaType.APPLICATION_OCTET_STREAM_TYPE, bytes)
                .build();
    }

    public String getChat_id() {
        return chat_id;
    }

    public void setChat_id(String chat_id) {
        this.chat_id = chat_id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

}
