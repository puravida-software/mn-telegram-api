package com.puravida.mn.telegram.util;

import com.puravida.mn.telegram.Update;
import io.micronaut.context.annotation.Requires;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Post;
import io.reactivex.Single;

@Requires(property = "telegram.controller", value = "true")
@Controller("/${telegram.token}")
public class TelegramController {

    DialogService dialogService;

    TelegramController(DialogService dialogService){
        this.dialogService = dialogService;
    }


    @Post
    boolean onMessage(final Update update){
        Single.create( emitter ->{
            if( update.getMessage() != null )
                dialogService.answer(update);
            else
                dialogService.answerCallback(update);
            emitter.onSuccess(true);
        }).subscribe();
        return true;
    }

}
