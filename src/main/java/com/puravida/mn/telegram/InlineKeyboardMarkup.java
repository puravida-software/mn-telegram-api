package com.puravida.mn.telegram;

import java.util.ArrayList;
import java.util.List;
import io.micronaut.core.annotation.Introspected;

@Introspected
public class InlineKeyboardMarkup extends ReplyMarkup{

    List<List<InlineKeyboardButton>> inline_keyboard = new ArrayList<>();

    public List<List<InlineKeyboardButton>> getInline_keyboard() {
        return inline_keyboard;
    }

    public void setInline_keyboard(List<List<InlineKeyboardButton>> inline_keyboard) {
        this.inline_keyboard = inline_keyboard;
    }

    public List<List<InlineKeyboardButton>> addToInline_keyboard(List<InlineKeyboardButton>add) {
        inline_keyboard.add(add);
        return inline_keyboard;
    }

}
