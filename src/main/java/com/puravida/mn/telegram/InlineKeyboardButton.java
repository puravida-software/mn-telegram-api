package com.puravida.mn.telegram;
import io.micronaut.core.annotation.Introspected;

@Introspected
public class InlineKeyboardButton {
    String text;
    String url;
    String login_url;
    String callback_data;
    String switch_inline_query;
    String switch_inline_query_current_chat;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogin_url() {
        return login_url;
    }

    public void setLogin_url(String login_url) {
        this.login_url = login_url;
    }

    public String getCallback_data() {
        return callback_data;
    }

    public void setCallback_data(String callback_data) {
        this.callback_data = callback_data;
    }

    public String getSwitch_inline_query() {
        return switch_inline_query;
    }

    public void setSwitch_inline_query(String switch_inline_query) {
        this.switch_inline_query = switch_inline_query;
    }

    public String getSwitch_inline_query_current_chat() {
        return switch_inline_query_current_chat;
    }

    public void setSwitch_inline_query_current_chat(String switch_inline_query_current_chat) {
        this.switch_inline_query_current_chat = switch_inline_query_current_chat;
    }
}
